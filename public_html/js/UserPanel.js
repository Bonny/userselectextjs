/* global Ext, Lang, failureCallback */

function Counter() {
  var sel = 0, all = 0;
  this.incrSel = function () {
    return sel++;
  };
  this.incrAll = function () {
    return all++;
  };
  this.print = function () {
    return "<div class=\"" + (sel > 0 ? (sel === all ? "all-selected" : "part-selected") : "") + "\">" + sel + " / " + all + "</div>";
  };
}

function TreeValue(user, admin, group, operator) {
  this.user = user;
  this.admin = admin;
  this.group = group;
  this.operator = operator;
  this.equals = function (treeValue) {
    return (count(Ext.Array.difference(treeValue.user, this.user)) === 0
            && count(Ext.Array.difference(this.user, treeValue.user)) === 0)
            &&
            (count(Ext.Array.difference(treeValue.admin, this.admin)) === 0
                    && count(Ext.Array.difference(this.admin, treeValue.admin)) === 0)
            &&
            (count(Ext.Array.difference(treeValue.group, this.group)) === 0
                    && count(Ext.Array.difference(this.group, treeValue.group)) === 0)
            &&
            (count(Ext.Array.difference(treeValue.operator, this.operator)) === 0
                    && count(Ext.Array.difference(this.operator, treeValue.operator)) === 0);
  };
}

var compareToString = function (s1, s2) {
  if (Ext.isEmpty(s1))
    s1 = "";
  if (Ext.isEmpty(s2))
    s2 = "";
  return s1.toUpperCase().localeCompare(s2.toUpperCase());
};

var treeSorterFn = function (o1, o2) {
  if (o1.get("leaf") && o2.get("leaf")) {
    if (o1.get("checked") && !o2.get("checked"))
      return -1;
    else if (!o1.get("checked") && o2.get("checked"))
      return 1;
    else
      return compareToString(o1.get("text"), o2.get("text"));
  } else if (!o1.get("leaf") && !o2.get("leaf")) {
    if (o1.get("checked") && !o2.get("checked"))
      return -1;
    else if (!o1.get("checked") && o2.get("checked"))
      return 1;
    else if (compareToString(o1.get("selMode"), "PART") === 0 && compareToString(o2.get("selMode"), "NONE") === 0)
      return -1;
    else if (compareToString(o1.get("selMode"), "NONE") === 0 && compareToString(o2.get("selMode"), "PART") === 0)
      return 1;
    else
      return compareToString(o1.get("text"), o2.get("text"));
  } else
    return 0;
};

Ext.define("user.tree.grouped", {
  extend: 'Ext.tree.Panel',
  alias: 'widget.usertreegrouped',
  rootVisible: false,
  isInit: false,
  initialValue: undefined,
  store: {
    xtype: 'tree',
    root: [],
    fields: ['text', {
        name: 'checked',
        defaultValue: false
      }, {
        name: 'idx',
        type: 'int'
      }, {
        name: 'admin',
        defaultValue: false
      }, {
        name: 'selMode',
        defaultValue: 'NONE' //NONE, ALL, PART
      }, {
        name: 'parentIdx',
        type: 'int'
      }, {
        name: 'operator',
        defaultValue: false
      }],
    remoteSort: false,
    sorters: [{
        sorterFn: treeSorterFn
      }]
  },
  constructor: function (config) {

    var me = this;

    config = extend({
      fk: 0,
      adminLabel: "Admin",
      url: 'GetUserOfMailbox',
      showAdminColumn: true,
      showOperatorColumn: true
    }, config);
    me.initConfig(config);

    var treeSearchFn = function () {
      me.getStore().clearFilter();
      var txt = Ext.getCmp("treeSearch").getValue();
      if (!Ext.isEmpty(txt)) {
        me.getStore().filter({
          filterFn: function (item) {
            return !item.get("leaf") && !Ext.isEmpty(item.get("text"))
                    ? item.get("text").toLowerCase().includes(txt.toLowerCase()) : true;
          }
        });
      }
    };

    var showAdm = me.getInitialConfig('showAdminColumn');
    var showOpt = me.getInitialConfig('showOperatorColumn');

    me.dockedItems = [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
          {xtype: 'label', cls: 'totalLabel', text: Lang.get('label.total.selected')},
          //
          {xtype: 'label', cls: 'totalLabel', text: Lang.get('label.groups')},
          {xtype: 'label', cls: 'totalLabel', text: '0', id: 'totalSelGroupLbl'},
          {xtype: 'label', cls: 'totalLabel', text: ' / '},
          {xtype: 'label', cls: 'totalLabel', text: '0', id: 'totalGroupLbl'},
          //
          {xtype: 'label', cls: 'totalLabel', text: ", " + Lang.get('label.users')},
          {xtype: 'label', cls: 'totalLabel', text: '0', id: 'totalSelUserLbl'},
          {xtype: 'label', cls: 'totalLabel', text: ' / '},
          {xtype: 'label', cls: 'totalLabel', text: '0', id: 'totalUserLbl'},
          //
          {xtype: 'label', hidden: !showAdm, cls: 'totalLabel', text: ", " + me.getInitialConfig('adminLabel')},
          {xtype: 'label', hidden: !showAdm, cls: 'totalLabel', text: '0', id: 'totalSelAdminLbl'},
          {xtype: 'label', hidden: !showAdm, cls: 'totalLabel', text: ' / '},
          {xtype: 'label', hidden: !showAdm, cls: 'totalLabel', text: '0', id: 'totalSelUserLbl2'},
          //
          {xtype: 'label', hidden: !showOpt, cls: 'totalLabel', text: ", " + Lang.get('label.operators')},
          {xtype: 'label', hidden: !showOpt, cls: 'totalLabel', text: '0', id: 'totalSelOperatorLbl'},
          {xtype: 'label', hidden: !showOpt, cls: 'totalLabel', text: ' / '},
          {xtype: 'label', hidden: !showOpt, cls: 'totalLabel', text: '0', id: 'totalSelUserLbl3'},
          , '->', {
            xtype: 'textfield',
            id: 'treeSearch',
            width: 150,
            emptyText: Lang.get("label.search.group") + '...',
            margin: '0 4 0 10',
            listeners: {
              specialkey: function (field, e) {
                if (e.getKey() === e.ENTER) {
                  treeSearchFn();
                }
              },
              afterrender: function () {
                this.setValue("");
                treeSearchFn();
              }
            }
          }, {
            xtype: 'button',
            text: Lang.get("label.search"),
            scale: 'medium',
            handler: treeSearchFn
          }
        ]
      }];

    var getCounter = function (idx, col) {
      var c = new Counter();
      me.getRootNode().cascadeBy(function (n) {
        if (n.get("idx") === idx && n.get("leaf") === false) {
          n.eachChild(function (user) {
            c.incrAll();
            if (user.get(col) === true)
              c.incrSel();
          });
        }
      });
      return c;
    };

    me.columns = [{
        xtype: 'treecolumn',
        text: Lang.get('label.groups'),
        flex: 1,
        sortable: true,
        dataIndex: 'text'
      }, {
        flex: 0.2,
        sortable: true,
        dataIndex: 'idx',
        align: 'center',
        text: Lang.get('label.users.selected'),
        renderer: function (idx, metadata) {
          return metadata.record.get("leaf") ? "" : getCounter(idx, "checked").print();
        }
      }, {
        flex: 0.2,
        sortable: true,
        dataIndex: 'idx',
        text: me.getInitialConfig('adminLabel'),
        hidden: !showAdm,
        align: 'center',
        renderer: function (idx, metadata, record, rowIndex, colIndex, store, view) {
          if (metadata.record.get("leaf")) {
            metadata.tdAttr = "align='center'";
            metadata.align = '';
            return "<div id=\"tg_admin_" + idx + "_" + record.id + "\"></div>";
          } else {
            metadata.align = 'center';
            return getCounter(idx, "admin").print();
          }
        }
      }, {
        flex: 0.2,
        sortable: true,
        dataIndex: 'idx',
        text: Lang.get("label.operator"),
        hidden: !showOpt,
        align: 'center',
        renderer: function (idx, metadata, record, rowIndex, colIndex, store, view) {
          if (metadata.record.get("leaf")) {
            metadata.tdAttr = "align='center'";
            metadata.align = '';
            return "<div id=\"tg_operator_" + idx + "_" + record.id + "\"></div>";
          } else {
            metadata.align = 'center';
            return getCounter(idx, "operator").print();
          }
        }
      }];

    me.initialValue = undefined;

    //end custom
    me.callParent([config]);
  },
  getValue: function () {
    var me = this,
            mapSel = new Ext.util.HashMap(),
            mapSelGrp = new Ext.util.HashMap(),
            mapAdm = new Ext.util.HashMap(),
            mapOpt = new Ext.util.HashMap();

    me.getRootNode().cascadeBy(function (n) {
      if (n.get("leaf") === true) {
        if (n.get("checked") === true)
          mapSel.add(n.get("idx"), {});
        if (n.get("admin") === true)
          mapAdm.add(n.get("idx"), {});
        if (n.get("operator") === true)
          mapOpt.add(n.get("idx"), {});
      } else if (n.get("checked") === true && parseInt(n.get("idx")) > 0) {
        mapSelGrp.add(n.get("idx"), {});
      }
    });

    return new TreeValue(mapSel.getKeys(), mapAdm.getKeys(), mapSelGrp.getKeys(), mapOpt.getKeys());
  },
  isChanged: function () {
    if (Ext.isEmpty(this.initialValue))
      return false;
    else
      return this.initialValue.equals(this.getValue()) ? false : true;
  },
  listeners: {
    render: function (that, eOpts) {
      //log("render");
      if (!that.isInit) {
        that.isInit = true;
        var mask = new Ext.LoadMask(that, {msg: Lang.get("loading.default.msg")});
        mask.show();
        Ext.Ajax.request({
          headers: {'Accept': 'application/json'},
          disableCaching: false,
          method: "GET",
          url: that.getInitialConfig('url'),
          failure: function failureCallback(form, action) {
            mask.hide();
            failureCallback(form, action);
          },
          success: function (response, opt) {
            mask.hide();
            response = Ext.decode(response.responseText);
            log("ajaxRequest success callback", response);
            if (response.success) {
              try {
                that.setRootNode(response.result);
                that.fireEvent("updateToolbarLabels", that);
                that.initialValue = that.getValue();
              } catch (e) {
                log("Errore durante la chiamata a ajaxRequest in success", e);
              }
            } else {
              try {
                errorCallback(response);
              } catch (err) {
                failureCallback();
              }
            }
          }
        });
      }
    },
    syncCheck: function (that, node, checked) {
      //log("syncCheck");
      that.getRootNode().cascadeBy(function (n) {
        if (n.get("leaf") === true && n.get("idx") === node.get("idx")) {
          n.set("checked", checked);
          n.commit();
        }
      });
      // syncView -> checkchange - altrimenti viene invocato N volte inutilmente
    },
    syncCheckAll: function (that) {
      //log("syncCheckAll");
      that.getRootNode().cascadeBy(function (n) {
        if (n.get("leaf") === false && parseInt(n.get("idx")) >= 0) {// discard root = -1  
          that.fireEvent("syncCheckGroup", that, n);
        }
      });
    },
    syncCheckGroup: function (that, parent) {
      //log("syncCheckGroup");
      var all = 0,
              sel = 0;
      that.getRootNode().cascadeBy(function (n) {
        if (n.get("leaf") === true && n.get("parentIdx") === parent.get("idx")) {
          all++;
          if (n.get("checked") === true)
            sel++;
        }
      });

      if (all > 0) {
        if (sel > 0 && sel === all && parent.get("checked") === false) {
          parent.set("checked", true);
          parent.commit();
        } else if (sel > 0 && sel !== all && parent.get("checked") === true) {
          parent.set("checked", false);
          parent.commit();
        }
      }
    },
    syncToggle: function (that, tg, col) {
      //log("syncToggle");
      that.getRootNode().cascadeBy(function (n) {
        if (n.get("leaf") === true && n.get("idx") === tg.idx) {
          n.set(col, tg.getValue());
          n.commit();
        }
      });
      that.fireEvent("syncView", that);
    },
    syncView: function (that) {
      //log("syncView");
      that.getStore().update();
      that.getView().refresh();
      that.fireEvent("updateToolbarLabels", that);
    },
    updateToolbarLabels: function (that) {
      //log("updateToolbarLabels");
      var mapSel = new Ext.util.HashMap(),
              mapTot = new Ext.util.HashMap(),
              mapAdm = new Ext.util.HashMap(),
              mapOpt = new Ext.util.HashMap(),
              groupTot = 0,
              groupSel = 0;
      that.getRootNode().cascadeBy(function (n) {
        mapTot.add(n.get("idx"), {});
        if (n.get("leaf") === true) {
          if (n.get("checked") === true)
            mapSel.add(n.get("idx"), {});
          if (n.get("admin") === true)
            mapAdm.add(n.get("idx"), {});
          if (n.get("operator") === true)
            mapOpt.add(n.get("idx"), {});
        } else if (parseInt(n.get("idx")) >= 0) {
          groupTot++;
          if (n.get("checked") === true)
            groupSel++;
        }
      });
      Ext.getCmp("totalSelUserLbl").setText(mapSel.getCount());
      Ext.getCmp("totalSelUserLbl2").setText(mapSel.getCount());
      Ext.getCmp("totalSelUserLbl3").setText(mapSel.getCount());
      Ext.getCmp("totalUserLbl").setText(mapTot.getCount());
      Ext.getCmp("totalSelAdminLbl").setText(mapAdm.getCount());
      Ext.getCmp("totalSelOperatorLbl").setText(mapOpt.getCount());
      Ext.getCmp("totalGroupLbl").setText(groupTot);
      Ext.getCmp("totalSelGroupLbl").setText(groupSel);
      mapSel.clear();
      mapTot.clear();
      mapAdm.clear();
    },
    afterlayout: function (that, layout, eOpts) {
      // log("afterlayout");
      var me = this;

      function _check(node, toggleId) {
        var cmp = Ext.getCmp(toggleId);
        if (node.get("checked") === true) {
          cmp.enable();
        } else {
          cmp.disable();
          if (cmp.getValue())
            cmp.toggle();
        }
      }

      me.getRootNode().cascadeBy(function (n) {

        if (n.get("leaf") === true) {

          var idx = n.get("idx"),
                  toggleContainerId = 'tg_admin_' + idx + "_" + n.id,
                  toggleId = 'tg_admin_cmp_' + idx + "_" + n.id;

          if (Ext.get(toggleContainerId)) {
            if (!Ext.get(toggleId)) {
              Ext.create('Ext.ux.toggleslide.ToggleSlide', {
                id: toggleId,
                onText: Lang.get("label.yes"),
                offText: Lang.get("label.no"),
                state: n.get("admin"),
                renderTo: toggleContainerId,
                booleanMode: true,
                idx: idx,
                listeners: {
                  change: function (that, state) {
                    me.fireEvent("syncToggle", me, that, "admin");
                  }
                }
              });
            } else {
              _check(n, toggleId);
            }
          }

          toggleContainerId = 'tg_operator_' + idx + "_" + n.id;
          toggleId = 'tg_operator_cmp_' + idx + "_" + n.id;

          if (Ext.get(toggleContainerId)) {
            if (!Ext.get(toggleId)) {
              Ext.create('Ext.ux.toggleslide.ToggleSlide', {
                id: toggleId,
                onText: Lang.get("label.yes"),
                offText: Lang.get("label.no"),
                state: n.get("operator"),
                renderTo: toggleContainerId,
                booleanMode: true,
                idx: idx,
                listeners: {
                  change: function (that, state) {
                    me.fireEvent("syncToggle", me, that, "operator");
                  }
                }
              });
            } else {
              _check(n, toggleId);
            }
          }

        }
      });
    },
    checkchange: function (node, checked) {
      //log("checkchange");
      var me = this;

      if (node.get("leaf")) {
        me.fireEvent("syncCheck", me, node, checked);
      } else {
        node.cascadeBy(function (child) {
          if (child.get("leaf") === true) {
            child.set('checked', checked);
            child.commit();
            me.fireEvent("syncCheck", me, child, checked);
          }
        });
      }

      me.fireEvent("syncCheckAll", me);
      me.fireEvent("syncView", me);
    }
  }
});

//FIXME
Lang.add("label.users.selected", "Selezionati");
Lang.add("label.total.selected", "Totale: ");
Lang.add("label.operator", "Operatore");
Lang.add("label.operators", "Operatori");
Lang.add("label.search", "Cerca");

Lang.add("label.groups", "Gruppi");
Lang.add("label.users", "Utenti");
Lang.add("label.search.group", "Cerca gruppo ..");

Ext.application({
  name: 'MyApp',
  launch: function () {



    Ext.create('Ext.window.Window', {
      title: 'Test',
      height: 400,
      width: 600,
      items: {
        xtype: "tabpanel",
        bodyPadding: 10,
        border: false,
        items: [{
            xtype: "panel"
          }, {
            xtype: 'usertreegrouped',
            id: 'panel-user-grouped-test',
            //url: 'GetUserOfMailbox?fk=' + 5 + '&_dc' + new Date().getTime().toString(),
            url: '../UserSelect/moke.json',
            layout: 'fit',
            border: true,
            preventHeader: true,
            height: 336
          }]
      },
      buttons: [{
          xtype: 'button',
          text: "Salva",
          handler: function () {
            log("isChanged="+Ext.getCmp('panel-user-grouped-test').isChanged());
            log(Ext.getCmp('panel-user-grouped-test').getValue());
          }
        }]
    }).show();
  }
});