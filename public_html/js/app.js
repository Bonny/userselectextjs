/* global Ext, data_tree, readerStore_model */

Ext.Loader.setConfig({
    enabled: true,
    // toglie _dc=
    disableCaching: false,
    paths: {
        'Ext.ux.toggleslide': 'extjs/4.2.3/plugins/ux/toggleslide',
        'Ext.ux.form.field': 'extjs/4.2.3/plugins/ux/form/field'
    }
});

Ext.require([
    'Ext.ux.form.field.ToggleSlide',
    'Ext.ux.toggleslide.Thumb'
]);