var Lang = new Ext.util.HashMap();
Lang.add("label.andCondition", "Soddisfano tutte le condizioni");
Lang.add("label.orCondition", "Soddisfano anche solo una condizionione");
Lang.add("label.allCondition", "Tutte incondizionatamente");
Lang.add("label.whenApplyFilter", "Quando applicare il filtro");
Lang.add("label.downloadNewMail", "Scarico i nuovi messaggi");
Lang.add("label.afterSent", "Dopo l'invio");
Lang.add('label.filterName', 'Nome filtro');
Lang.add("label.active", "Attivo");
Lang.add("label.notActive", "Non attivo");
Lang.add("label.status", "Stato");
Lang.add("label.yes", "Si");
Lang.add("label.no", "No");


Lang.add("label.groups", "Gruppi");
Lang.add("label.users.selected", "Selezionati");
Lang.add("label.total.selected", "Totale selezionati: ");


var readerStore_model = {type: 'json', root: 'result', totalProperty: 'total', successProperty: 'success', msgProperty: 'reason'};

function extend(destination, source) {
   for (var property in source) {
      destination[property] = source[property];
   }
   return destination;
}

function count(array) {
   return !Ext.isEmpty(array) ? array.length : 0;
}

function log(arg0, arg1) {
   try {
      if (!console)
         console = {log: function (c) {
            }};
      console.log(arg0);
      Ext.log({level: 'debug'}, arg0);
      if (arg1) {
         console.log(arg1);
         Ext.log({level: 'debug'}, arg1);
      }
   } catch (err) {

   }
}

function mask() {
  // Ext.getBody().mask(Lang.get("loading.default.msg"));
   Ext.getBody().mask("Attendere..");
}

function unmask() {
   Ext.getBody().unmask();
}

var TOT_USERS = 250;
var TOT_USER_CHECK = 40;
var TOT_GROUPS = 40;
var MAX_USER_FOR_GROUP = 20;

function genUsers() {

    var v = [],
            i,
            j;

    for (i = 1; i <= TOT_USERS; i++) {
        v.push({
            "expanded": false,
            "children": [],
            "checked": false,
            "text": "user.name" + i,
            "iconCls": "user",
            "leaf": true,
            "idx": i,
            "qtip": null,
            "admin": false
        });
    }

    for (i = 0; i < TOT_USER_CHECK; i++) {
        j = Math.floor(Math.random() * TOT_USERS) + 1;
        v[j]["checked"] = true;
        v[j]["admin"] = i % 2 === 0;
    }

    return v;
}

function genGroups() {

    var u = genUsers(),
            i,
            v = [],
            g = {},
            j,
            RANGE,
            children,
            c;

    for (i = 1; i < TOT_GROUPS; i++) {

        g = {
            "expanded": false,
            "children": [],
            "checked": false,
            "text": "Ufficio group.name" + i,
            "iconCls": "group",
            "leaf": false,
            "idx": i,
            "qtip": null,
            "admin": false
        };

        RANGE = (Math.floor(Math.random() * MAX_USER_FOR_GROUP) + 1);

        children = [];
        for (j = 1; j <= RANGE; j++)
            children.push(u[j]);

        c = 0;
        for (j = 0; j < count(children); j++)
            if (children[j]["checked"])
                c++;

        g["children"] = children;
        g["checked"] = c === count(children);

        v.push(g);
    }

//    v[0]["checked"] = true;
//    for (i = 0; i < count(v[0].children); i++) {
//        v[0].children[i]["checked"] = true;
//    }

    return v;
}


var data_tree = {
    "expanded": true,
    //"children": genGroups(),
    "children": [{"expanded":false,"children":[{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":false,"admin":false,"text":"user02","leaf":true,"iconCls":"user","idx":3,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""}],"checked":false,"admin":false,"text":"admin group","leaf":false,"iconCls":"group","idx":1,"qtip":""},{"expanded":false,"children":[{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":false,"admin":false,"text":"user02","leaf":true,"iconCls":"user","idx":3,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""}],"checked":false,"admin":false,"text":"GRUPPO 01","leaf":false,"iconCls":"group","idx":5,"qtip":""},{"expanded":false,"children":[{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":false,"admin":false,"text":"user02","leaf":true,"iconCls":"user","idx":3,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"admin","leaf":true,"iconCls":"user","idx":1,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":false,"text":"user01","leaf":true,"iconCls":"user","idx":2,"qtip":""},{"expanded":false,"children":[],"checked":true,"admin":true,"text":"user05","leaf":true,"iconCls":"user","idx":6,"qtip":""}],"checked":false,"admin":false,"text":"GRUPPO 02","leaf":false,"iconCls":"group","idx":6,"qtip":""}],
    "checked": false,
    "text": "root",
    "iconCls": null,
    "leaf": false,
    "idx": -1,
    "qtip": null
};